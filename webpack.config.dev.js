const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	devtool: 'eval',
	devServer: {
		contentBase: path.join(__dirname, 'build'),
		publicPath: '/public',
		open: true,
		historyApiFallback: true,
		hot: true
	},
	entry: [
		path.join(__dirname, 'src/app/index.js'),
		'webpack/hot/only-dev-server',
		'react-hot-loader/patch'
	],
	output: {
		path: path.join(__dirname, 'build'),
		filename: 'app/[name].js',
		publicPath: '/public'
	},
	module: {
		rules: [
			{
				test: /\.js$|\.jsx$/,
				include: path.join(__dirname, 'src/app'),
				use: [
					'babel-loader'
				]
			}, {
				test: /\.css$|\.scss$/,
				include: path.join(__dirname, 'src/styles'),
				use: [
					'style-loader',
					'css-loader',
					'sass-loader'
				]
			}, {
				test: /\.(eot|svg|ttf|woff|woff2)$/,
				include: path.join(__dirname, 'src/assets/icons'),
				use: [
					{
						loader: 'file-loader',
						options: {
							name: 'assets/icons/[name].[ext]'
						}
					}
				]
			}
		]
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new HtmlWebpackPlugin({
			template: path.join(__dirname, 'src/templates/index.ejs')
		})
	],
	resolve: {
		extensions: [
			'.js',
			'.jsx',
			'.css',
			'.scss',
			'.html',
			'.ejs'
		],
		mainFields: [
			'browser',
			'esnext',
			'main'
		]
	}
};
