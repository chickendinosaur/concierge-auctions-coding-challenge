const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	entry: [
		path.join(__dirname, 'src/app/index.js')
	],
	output: {
		path: path.join(__dirname, 'build'),
		filename: 'app/[name].[chunkhash].js',
		chunkFilename: 'app/[name].[chunkhash].js'
	},
	module: {
		rules: [
			{
				test: /\.js$|\.jsx$/,
				include: path.join(__dirname, 'src/app'),
				loaders: ['babel-loader']
			}, {
				test: /\.css$|\.scss$/,
				include: path.join(__dirname, 'src/styles'),
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: [
						{
							loader: 'css-loader',
							query: {
								minimize: true
							}
						},
						{
							loader: 'sass-loader'
						}
					]
				})
			}, {
				test: /\.(eot|svg|ttf|woff|woff2)$/,
				include: path.join(__dirname, 'src/assets/icons'),
				use: [
					{
						loader: 'file-loader',
						options: {
							name: 'assets/icons/[name].[ext]'
						}
					}
				]
			}
		]
	},
	plugins: [
		new ExtractTextPlugin('styles/[name].[hash].css'),
		new webpack.optimize.CommonsChunkPlugin({
			name: 'vendor',
			minChunks: Infinity
		}),
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.optimize.UglifyJsPlugin({
			compress: {
				warnings: false
			},
			mandle: true,
			comments: false
		}),
		new webpack.optimize.AggressiveMergingPlugin({
			minSizeReduce: 1.5
		}),
		new HtmlWebpackPlugin({
			template: path.join(__dirname, 'src/templates/index.ejs'),
			minify: {
				collapseInlineTagWhitespace: true,
				collapseWhitespace: true
			}
		})
	],
	resolve: {
		extensions: [
			'.js',
			'.jsx',
			'.css',
			'.scss',
			'.html',
			'.ejs'
		],
		mainFields: [
			'browser',
			'esnext',
			'main'
		],
		modules: [
			'node_modules'
		]
	}
};
