export default function reducer(state, action) {
  switch (action.type) {
    case 'search':
      // Should be:
      // return {
      //   ...state
      // }
      // but the linter does not like the ...
      // linting over developing?
      return {
        favorites: state.favorites,
        repos: action.payload.data.items
      };
    case 'getSearchedRepos':
      return {
        favorites: state.favorites,
        repos: action.payload
      };
    case 'getFavorites':
      return {
        repos: state.repos,
        favorites: action.payload
      };
    case 'deleteFavorite':
      return {
        repos: state.repos,
        favorites: action.payload
      };
    default:
  }
  return state;
}
