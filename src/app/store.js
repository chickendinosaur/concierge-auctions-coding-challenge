import { createStore, applyMiddleware } from 'redux';
import createLogger from 'redux-logger';
import rootReducer from './reducers';

const initialState = {
  repos: [],
  favorites: []
};

if (localStorage.appState === undefined) {
  localStorage.appState = JSON.stringify(initialState);
}

export default createStore(
  rootReducer,
  initialState,
  applyMiddleware(
    createLogger()
  )
);
