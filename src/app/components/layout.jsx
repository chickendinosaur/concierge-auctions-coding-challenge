import React from 'react';
import LayoutHeader from '../containers/layout-toolbar.jsx';

export default function Layout({ children }) {
  return (
    <div className="layout">
      <LayoutHeader />
      {children}
    </div>
  );
}

Layout.propTypes = {
  children: React.PropTypes.node
};
