'use strict';

import React, { Component } from 'react';
import moment from 'moment';

export default class RepoCard extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.handleDeleteRef = this.handleDelete.bind(this);
  }

  render() {
    // Limit commits to 3.
    let commits = null;
    if (this.props.commits) {
      commits = [];
      for (let i = 0; i < 3 && i < this.props.commits.length; i++) {
        let item = this.props.commits[i];
        let authorURL = item.author ? item.author.html_url : 'https://github.com/' + item.commit.url.split('/')[5]
        commits.push(
          <Commit
            key={item.sha}
            authorName={item.commit.author.name}
            authorURL={authorURL}
            message={item.commit.message}
            date={item.commit.committer.date}
          />
        );
      }
    }

    return (
      <div className="repo-card">
        <div className="header">
      <a href={`https://github.com/${this.props.fullName}`} className="repo-name">
        {this.props.fullName}
      </a>
      <div
        className="remove-btn"
        onClick={this.handleDeleteRef}
         />
    </div>
    <div className="body">
      <div className="stars">
        <div className="star-icon">
        </div>
        <label>{this.props.starsCount}</label>
      </div>
    </div>
        <div className="commits">
          <span className="title">
            Commits
          </span>
          {commits}
        </div>
    </div>
    );
  }

  handleDelete(event) {
    this.props.onDelete(this.props.fullName)
  }
}

RepoCard.propTypes = {
  fullName: React.PropTypes.string,
  starsCount: React.PropTypes.number,
  commits: React.PropTypes.array,
  onDelete: React.PropTypes.func
};

function Commit(props) {
  // Convert to days.
  let dateDiff = ~~(moment().diff(moment(props.date)) / 1000 / 60 / 60 / 24);
  return (
    <div className="repo-card_commit">
      <a href={props.authorURL}>
        {props.authorName}
      </a>
      <p className="message">
        {props.message}
      </p>
      <label className="time-diff">
        {dateDiff} days ago.
      </label>
  </div>
  );
}

Commit.propTypes = {
  authorName: React.PropTypes.string,
  authorURL: React.PropTypes.string,
  message: React.PropTypes.string,
  date: React.PropTypes.string
};
