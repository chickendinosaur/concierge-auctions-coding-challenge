'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import AutoComplete from 'material-ui/AutoComplete';
import MenuItem from 'material-ui/MenuItem';
import { searchReposAction } from '../actions/repos';
import { getSearchedReposAction } from '../actions/repos';
import { addFavoriteAction } from '../actions/favorites';

class LayoutToolbar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchQuery: ''
    };

    this.handleSearchRequestRef = this.handleSearchRequest.bind(this);
  }

  componentWillMount() {
    getSearchedReposAction();
  }

  // AutoComplete does not work as advertised and is poorly implemented to be
  // able to do what you want. After the internet and banging my head
  // I just picked a way to be able to select the a repo and add to favorites.
  // Can't close the menu after meny click due to the 'open' property not working
  // or onNewRequest working to spec. Nothing worse the buggy/poorly made third-party
  // components when your sicks. This one component along with how they're inlining styles
  // showed me a enough to never look at react material-ui again.
  render() {
    return (
		<div className="layout-toolbar">
			<label className="title">
        <label className="github">
          Github
        </label>
        <label className="name">
          Favorites
        </label>
      </label>
			<div className="search-container">
				<AutoComplete
					dataSource={this.props.repos.map((item, index) => {
						return {
							text: item.full_name,
							value:
								<MenuItem
									onClick={this.handleItemSelect.bind(this, item)}
									primaryText={item.full_name}
									data-item={index}
								/>
						};
					})}
					dataSourceConfig={{
						text: 'text',
						value: 'value'
					}}
					maxSearchResults={8}
					inputStyle={{color: 'white'}}
					hintStyle={{color: 'gray'}}
					floatingLabelStyle={{color: 'gray'}}
					floatingLabelText="Search"
					hintText="chickendinosaur/storex"
					fullWidth={true}
					openOnFocus={true}
					onNewRequest={this.handleSearchRequestRef}
				/>
			</div>
		</div>
    );
  }

  handleSearchRequest(queryStr, index) {
    if (index >= 0) {
      addFavoriteAction(this.props.repos[index])
    } else if (queryStr.length > 0 &&
      this.state.searchQuery !== queryStr) {
      this.setState({
        searchQuery: queryStr
      });

      searchReposAction(queryStr)
        .then((res) => {
          if (res.data.items[0].name === queryStr) {
            // Add the top item searched to favorties right away
            // since it's a match.
            addFavoriteAction(res.data.items[0]);
          }
        });
    }
  }

  handleItemSelect(item) {
    addFavoriteAction(item);
  }
}

LayoutToolbar.propTypes = {
  repos: React.PropTypes.array
};

const mapStateToProps = function (state) {
  return {
    repos: state.repos
  };
};

const LayoutToolbarContaner = connect(
  mapStateToProps
)(LayoutToolbar);

export default LayoutToolbarContaner;
