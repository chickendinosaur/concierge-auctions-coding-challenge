import React, { Component } from 'react';
import { connect } from 'react-redux';
import RepoCard from '../components/repo-card.jsx';
import { deleteFavoriteAction } from '../actions/favorites';
import { getFavoritesAction } from '../actions/favorites';

class Favorites extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    getFavoritesAction();
  }

  render() {
    return (
      <div className="favorites">
			{
				this.props.favorites.map((item) => {
					return (
						<RepoCard
							key={item.full_name}
							fullName={item.full_name}
							starsCount={item.stargazers_count}
              commits={item.commits}
							onDelete={this.handleDeleteFavorite}
						/>
					);
				})
			}
			</div>
    );
  }

  handleDeleteFavorite(fullName) {
    deleteFavoriteAction(fullName);
  }
}

Favorites.propTypes = {
  favorites: React.PropTypes.array
};

const mapStateToProps = function (state) {
  return {
    favorites: state.favorites
  };
};

const FavoritesContainer = connect(
  mapStateToProps
)(Favorites);

export default FavoritesContainer;
