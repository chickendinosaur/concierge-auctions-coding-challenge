import React from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import store from './store';

import Layout from './components/layout.jsx';
import FavoritesContainer from './containers/favorites.jsx';

export default function App() {
  return (
    <MuiThemeProvider>
		<Provider store={store}>
			<Router history={browserHistory}>
				<Route path='/' component={Layout}>
				<IndexRoute component={FavoritesContainer} />
				</Route>
			</Router>
		</Provider>
	</MuiThemeProvider>
  );
}
