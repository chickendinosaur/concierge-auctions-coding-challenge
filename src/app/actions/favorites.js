import store from '../store';
import axios from '../axios';

export function getFavoritesAction() {
  /*
  TODO: Refresh repo data for current favorites.
  Should store the repo names only in localStorage and pull re-pull on load.
  Out of time!
  */

  const localAppState = JSON.parse(localStorage.appState);
  const favorites = localAppState.favorites;
  const promises = [];

  let i = 0;
  const n = favorites.length;
  for (i; i < n; i++) {
    promises.push(new Promise((resolve, reject) => {
      const index = i;
      let commitsURL = favorites[i].commits_url;
      commitsURL = commitsURL.substring(0, commitsURL.indexOf('{'));
      axios.get(commitsURL)
        .then((res) => {
          favorites[index].commits = res.data;
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    }));
  }

  Promise.all(promises)
    .then(() => {
      localStorage.appState = JSON.stringify(localAppState);

      store.dispatch({
        type: 'getFavorites',
        payload: favorites
      });
    });
}

export function addFavoriteAction(repo) {
  const localAppState = JSON.parse(localStorage.appState);

  // Check for duplicates.
  let i;
  const n = localAppState.favorites.length;
  let dupeFound = false;
  for (i = 0; i < n; i++) {
    if (localAppState.favorites[i].full_name === repo.full_name) {
      dupeFound = true;
      break;
    }
  }

  if (dupeFound === false) {
    localAppState.favorites.push(repo);
    localStorage.appState = JSON.stringify(localAppState);

    getFavoritesAction();
  }
}

export function deleteFavoriteAction(fullName) {
  const localAppState = JSON.parse(localStorage.appState);

  // Check for duplicates.
  let i;
  const n = localAppState.favorites.length;
  for (i = 0; i < n; i++) {
    if (localAppState.favorites[i].full_name === fullName) {
      localAppState.favorites.splice(i, 1);
      localStorage.appState = JSON.stringify(localAppState);

      store.dispatch({
        type: 'deleteFavorite',
        payload: localAppState.favorites
      });
      break;
    }
  }
}
