import store from '../store';
import axios from '../axios';

export function getSearchedReposAction() {
  const localAppState = JSON.parse(localStorage.appState);

  store.dispatch({
    type: 'getSearchedRepos',
    payload: localAppState.repos
  });
}

export function searchReposAction(searchQuery) {
  let sq = searchQuery;
  const namespaceIndex = searchQuery.indexOf('/');
  let username = null;
  if (namespaceIndex >= 0) {
    username = searchQuery.substring(0, namespaceIndex);
    sq = searchQuery.substring(namespaceIndex + 1);
  }

  // Search user repos.
  if (username !== null) {
    sq += ` user:${username}`;
  }

  return axios.get('https://api.github.com/search/repositories', {
    params: {
      q: sq,
      order: 'asc'
    }
  })
  .then((res) => {
    const localAppState = JSON.parse(localStorage.appState);
    localAppState.repos = res.data.items;
    localStorage.appState = JSON.stringify(localAppState);

    store.dispatch({
      type: 'search',
      payload: res
    });
    return res;
  })
  .catch((err) => {
    store.dispatch({
      type: 'ssearchError',
      payload: err
    });
  });
}
